import sun.misc.Version

object ApplicationId {
    val appId = "com.glucorx.android"
}

object Release {
    val versionCode = 1
    val versionName = "1.0"
}

object Versions {

    val compileSdkVersion = 29
    val buildToolsVersion = "29.0.2"
    val minSdkVersion = 23
    val targetSdkVersion = 29

    //Support
    val appcompat = "1.1.0"
    val ktx = "1.2.0-alpha04"
    val preference = "1.1.0"
    val constraintlayout = "2.0.0-beta2"
    val multidex = "2.0.1"
    val material = "1.1.0-alpha10"
    val cardview = "1.0.0"
    val recyclerview = "1.0.0"

    //Kotlin
    val kotlin = "1.3.50"

    //Android Architecture Components
    val lifecycles = "2.1.0"
    val room = "2.2.0-rc01"
    val roomLegacySupport = "1.0.0"
    val navigationUI = "2.1.0"

    //Dagger
    val dagger = "2.24"

    //RX
    val rxJava = "2.2.12"
    val rxKotlin = "2.3.0"
    val rxAndroid = "2.1.0"

    //Retrofit
    val retrofit = "2.5.0"

    //Volley
    val volley = "1.1.0"

    //OKhttp
    val okhttp = "3.14.0"

    //Logging Interceptor
    val loggingInterceptor = "3.0.0"
    val okhttpLoggingInterceptor = "3.8.0"

    //gson
    val gson = "2.8.5"

    //Glide
    val glide = "4.8.0"

    //Circle image view
    val circleimageview = "3.0.0"


    //Picasso
    val picasso = "2.71828"

    //Jwt Parser
    val jwtdecode = "1.1.1"


    val awsVersion = "2.16.0"
    val awsAppSyncVersion = "2.10.0"
}

object Libraries {

    //Support
    val appCompat = "androidx.appcompat:appcompat:${Versions.appcompat}"
    val ktx = "androidx.core:core-ktx:${Versions.ktx}"
    val preference = "androidx.preference:preference:${Versions.preference}"
    val constraintlayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintlayout}"
    val multidex = "androidx.multidex:multidex:${Versions.multidex}"
    val material = "com.google.android.material:material:${Versions.material}"
    val cardview = "androidx.cardview:cardview:${Versions.cardview}"
    val recyclerview = "androidx.recyclerview:recyclerview:${Versions.recyclerview}"

    //kotlin
    val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"
    val kotlinReflect = "org.jetbrains.kotlin:kotlin-reflect:${Versions.kotlin}"

    //Android Architecture Components: lifecycle
    val lifecycles = "androidx.lifecycle:lifecycle-extensions:${Versions.lifecycles}"
    val lifecycleCommon = "androidx.lifecycle:lifecycle-common-java8:${Versions.lifecycles}"
    val lifecycleReactivestreams = "androidx.lifecycle:lifecycle-reactivestreams:${Versions.lifecycles}"
    val lifecycleViewmodel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycles}"
    val lifecyclesCompiler = "androidx.lifecycle:lifecycle-compiler:${Versions.lifecycles}"

    //Android Architecture Components: room
    val roomRuntime = "androidx.room:room-runtime:${Versions.room}"
    val roomRxjava2 = "androidx.room:room-rxjava2:${Versions.room}"
    val roomLegacySupport = "androidx.legacy:legacy-support-v4:${Versions.roomLegacySupport}"
    val roomCompiler = "androidx.room:room-compiler:${Versions.room}"

    //Architecture Components : navigationUI  Kotlin
    val navigationFragment = "androidx.navigation:navigation-fragment-ktx:${Versions.navigationUI}"
    val navigationUiKtx = "androidx.navigation:navigation-ui-ktx:${Versions.navigationUI}"

    //Dagger - dependency injector
    val dagger =  "com.google.dagger:dagger:${Versions.dagger}"
    val daggerCompiler = "com.google.dagger:dagger-compiler:${Versions.dagger}"
    val daggerAndroid = "com.google.dagger:dagger-android:${Versions.dagger}"
    val daggerAndroidSupport =  "com.google.dagger:dagger-android-support:${Versions.dagger}"
    val daggerAndroidCompiler = "com.google.dagger:dagger-android-processor:${Versions.dagger}"

    // Rx
    val rxJava =  "io.reactivex.rxjava2:rxjava:${Versions.rxJava}"
    val rxAndroid =  "io.reactivex.rxjava2:rxandroid:${Versions.rxAndroid}"
    val rxKotlin = "io.reactivex.rxjava2:rxkotlin:${Versions.rxKotlin}"

    // AWS
    val awsCore = "com.amazonaws:aws-android-sdk-core:${Versions.awsVersion}"
    val awsMobileClient = "com.amazonaws:aws-android-sdk-mobile-client:${Versions.awsVersion}"
    val awsAppSync = "com.amazonaws:aws-android-sdk-appsync:${Versions.awsAppSyncVersion}"

    // retrofit https://github.com/square/retrofit
    val retrofit =  "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    val retrofitConverter = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
    val retrofitRXAdapter = "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofit}"

    //Volley
    val volley = "com.android.volley:volley:${Versions.volley}"

    // https://github.com/square/okhttp
    val okHttp = "com.squareup.okhttp3:okhttp:${Versions.okhttp}"
    val okhttpLoggingInterceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.okhttpLoggingInterceptor}"
    val loggingInterceptor = "com.github.ihsanbal:LoggingInterceptor:${Versions.loggingInterceptor}"

    //gson
    val gson = "com.google.code.gson:gson:${Versions.gson}"

    // Glide
    val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    val glideCompiler = "com.github.bumptech.glide:compiler:${Versions.glide}"
    val glideOkHttp = "com.github.bumptech.glide:okhttp3-integration:${Versions.glide}"

    //Circle image view
    val circleimageview = "de.hdodenhof:circleimageview:${Versions.circleimageview}"

    //Picasso
    val picasso = "com.squareup.picasso:picasso:${Versions.picasso}"

    //Jwt Parser
    val jwtdecode = "com.auth0.android:jwtdecode:${Versions.jwtdecode}"

}